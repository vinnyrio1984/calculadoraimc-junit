package Calculadora;

public class CalculadoraIMC {

	public String calcularIMC(double altura, double peso, String sexo, int idade) {
		double imc = peso / (altura * altura);

		// ADULTOS
		if (idade > 20 && idade <= 65) {
			if (imc < 16)
				return "Baixo Peso - Muito Grave";
			else if (imc >= 16 && imc < 17)
				return "Baixo Peso - Grave";
			else if (imc >= 17 && imc < 18.50)
				return "Baixo Peso";
			else if (imc >= 18.50 && imc < 25)
				return "Peso Ideal";
			else if (imc >= 25 && imc < 30)
				return "Sobrepeso";
			else if (imc >= 30 && imc < 35)
				return "Obesidade G I";
			else if (imc >= 35 && imc < 40)
				return "Obesidade G II";
			else if (imc >= 40)
				return "Obesidade G III";
			// IDOSOS FEMININO
		} else if (idade > 65 && sexo == "F") {
			if (imc < 22)
				return "Baixo Peso";
			else if (imc >= 22 && imc < 27.1)
				return "Peso Ideal";
			else if (imc >= 27.1 && imc < 32.1)
				return "Sobrepeso";
			else if (imc >= 32.1 && imc < 37.1)
				return "Obesidade G I";
			else if (imc >= 37.1 && imc < 42)
				return "Obesidade G II";
			else if (imc >= 42)
				return "Obesidade G III";
			// IDOSOS MASCULINO
		} else if (idade > 65 && sexo == "M") {
			if (imc < 22)
				return "Baixo Peso";
			else if (imc >= 22 && imc < 27.1)
				return "Peso Ideal";
			else if (imc >= 27.1 && imc < 30.1)
				return "Sobrepeso";
			else if (imc >= 30.1 && imc < 35.1)
				return "Obesidade G I";
			else if (imc >= 35.1 && imc < 40)
				return "Obesidade G II";
			else if (imc >= 40)
				return "Obesidade G III";
			// INFANTIL FEMININO
		} else if (idade <= 20 && sexo == "F") {
			if (idade >= 2 && idade <= 4) {
				if (imc < 14.01 || (imc >= 14.01 && imc < 14.81)) {
					return "Baixo Peso";
				} else if (imc >= 14.81 && imc < 17.41) {
					return "Peso Ideal";
				} else if (imc >= 17.41 && imc < 19.01) {
					return "Sobrepeso";
				} else if (imc >= 19.01) {
					return "Obesidade";
				}
			} else if (idade > 4 && idade <= 6) {
				if (imc < 13.21 || (imc >= 13.21 && imc < 13.81)) {
					return "Baixo Peso";
				} else if (imc >= 13.81 && imc < 16.41) {
					return "Peso Ideal";
				} else if (imc >= 16.41 && imc < 18.21) {
					return "Sobrepeso";
				} else if (imc >= 18.21) {
					return "Obesidade";
				}
			} else if (idade > 6 && idade <= 8) {
				if (imc < 13.21 || (imc >= 13.21 && imc < 13.81)) {
					return "Baixo Peso";
				} else if (imc >= 13.81 && imc < 16.61) {
					return "Peso Ideal";
				} else if (imc >= 16.61 && imc < 19.41) {
					return "Sobrepeso";
				} else if (imc >= 19.41) {
					return "Obesidade";
				}
			} else if (idade > 8 && idade <= 10) {
				if (imc < 13.41 || (imc >= 13.41 && imc < 14.01)) {
					return "Baixo Peso";
				} else if (imc >= 14.01 && imc < 18.01) {
					return "Peso Ideal";
				} else if (imc >= 18.01 && imc < 21.81) {
					return "Sobrepeso";
				} else if (imc >= 21.81) {
					return "Obesidade";
				}
			} else if (idade > 10 && idade <= 12) {
				if (imc < 14.01 || (imc >= 14.01 && imc < 15.01)) {
					return "Baixo Peso";
				} else if (imc >= 15.01 && imc < 19.21) {
					return "Peso Ideal";
				} else if (imc >= 19.21 && imc < 24.01) {
					return "Sobrepeso";
				} else if (imc >= 24.01) {
					return "Obesidade";
				}
			} else if (idade > 12 && idade <= 14) {
				if (imc < 15.01 || (imc >= 15.01 && imc < 16.01)) {
					return "Baixo Peso";
				} else if (imc >= 16.01 && imc < 21.01) {
					return "Peso Ideal";
				} else if (imc >= 21.01 && imc < 26.21) {
					return "Sobrepeso";
				} else if (imc >= 26.21) {
					return "Obesidade";
				}
			} else if (idade > 14 && idade <= 16) {
				if (imc < 16.01 || (imc >= 16.01 && imc < 17.01)) {
					return "Baixo Peso";
				} else if (imc >= 17.01 && imc < 22.21) {
					return "Peso Ideal";
				} else if (imc >= 22.21 && imc < 28.01) {
					return "Sobrepeso";
				} else if (imc >= 28.01) {
					return "Obesidade";
				}
			} else if (idade > 16 && idade <= 18) {
				if (imc < 16.81 || (imc >= 16.81 && imc < 18.01)) {
					return "Baixo Peso";
				} else if (imc >= 18.01 && imc < 23.41) {
					return "Peso Ideal";
				} else if (imc >= 23.41 && imc < 29.61) {
					return "Sobrepeso";
				} else if (imc >= 29.61) {
					return "Obesidade";
				}
			} else if (idade > 18 && idade <= 20) {
				if (imc < 17.21 || (imc >= 17.21 && imc < 18.41)) {
					return "Baixo Peso";
				} else if (imc >= 18.41 && imc < 24.21) {
					return "Peso Ideal";
				} else if (imc >= 24.21 && imc < 31.01) {
					return "Sobrepeso";
				} else if (imc >= 31.01) {
					return "Obesidade";
				}
			}
			// INFANTIL MASCULINO
		} else if (idade <= 20 && sexo == "M") {
			if (idade >= 2 && idade <= 4) {
				if (imc < 14.01 || (imc >= 14.01 && imc < 14.81)) {
					return "Baixo Peso";
				} else if (imc >= 14.81 && imc < 16.81) {
					return "Peso Ideal";
				} else if (imc >= 16.81 && imc < 18.21) {
					return "Sobrepeso";
				} else if (imc >= 18.21) {
					return "Obesidade";
				}
			} else if (idade > 4 && idade <= 6) {
				if (imc < 13.61 || (imc >= 13.61 && imc < 14.21)) {
					return "Baixo Peso";
				} else if (imc >= 14.21 && imc < 16.21) {
					return "Peso Ideal";
				} else if (imc >= 16.21 && imc < 18.81) {
					return "Sobrepeso";
				} else if (imc >= 18.81) {
					return "Obesidade";
				}
			} else if (idade > 6 && idade <= 8) {
				if (imc < 13.61 || (imc >= 13.61 && imc < 14.01)) {
					return "Baixo Peso";
				} else if (imc >= 14.01 && imc < 16.81) {
					return "Peso Ideal";
				} else if (imc >= 16.81 && imc < 19.01) {
					return "Sobrepeso";
				} else if (imc >= 19.01) {
					return "Obesidade";
				}
			} else if (idade > 8 && idade <= 10) {
				if (imc < 13.81 || (imc >= 13.81 && imc < 14.41)) {
					return "Baixo Peso";
				} else if (imc >= 14.41 && imc < 17.41) {
					return "Peso Ideal";
				} else if (imc >= 17.41 && imc < 21.01) {
					return "Sobrepeso";
				} else if (imc >= 21.01) {
					return "Obesidade";
				}
			} else if (idade > 10 && idade <= 12) {
				if (imc < 14.21 || (imc >= 14.21 && imc < 15.01)) {
					return "Baixo Peso";
				} else if (imc >= 15.01 && imc < 19.01) {
					return "Peso Ideal";
				} else if (imc >= 19.01 && imc < 23.01) {
					return "Sobrepeso";
				} else if (imc >= 23.01) {
					return "Obesidade";
				}
			} else if (idade > 12 && idade <= 14) {
				if (imc < 15.01 || (imc >= 15.01 && imc < 16.01)) {
					return "Baixo Peso";
				} else if (imc >= 16.01 && imc < 20.21) {
					return "Peso Ideal";
				} else if (imc >= 20.21 && imc < 25.01) {
					return "Sobrepeso";
				} else if (imc >= 25.01) {
					return "Obesidade";
				}
			} else if (idade > 14 && idade <= 16) {
				if (imc < 16.21 || (imc >= 16.21 && imc < 17.01)) {
					return "Baixo Peso";
				} else if (imc >= 17.01 && imc < 22.01) {
					return "Peso Ideal";
				} else if (imc >= 22.01 && imc < 26.81) {
					return "Sobrepeso";
				} else if (imc >= 26.81) {
					return "Obesidade";
				}
			} else if (idade > 16 && idade <= 18) {
				if (imc < 17.21 || (imc >= 17.21 && imc < 18.21)) {
					return "Baixo Peso";
				} else if (imc >= 18.21 && imc < 23.41) {
					return "Peso Ideal";
				} else if (imc >= 23.41 && imc < 28.21) {
					return "Sobrepeso";
				} else if (imc >= 28.21) {
					return "Obesidade";
				}
			} else if (idade > 18 && idade <= 20) {
				if (imc < 18.21 || (imc >= 18.21 && imc < 19.41)) {
					return "Baixo Peso";
				} else if (imc >= 19.41 && imc < 24.81) {
					return "Peso Ideal";
				} else if (imc >= 24.81 && imc < 29.61) {
					return "Sobrepeso";
				} else if (imc >= 29.61) {
					return "Obesidade";
				}
			}
		}
		return "";
	}
}
