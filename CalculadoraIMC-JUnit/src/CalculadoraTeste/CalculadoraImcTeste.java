package CalculadoraTeste;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import Calculadora.CalculadoraIMC;

public class CalculadoraImcTeste {

	private CalculadoraIMC imc;

	@BeforeClass
	protected void setUp() {
		imc = new CalculadoraIMC();
	}
	//Casos de Testes da categoria Adultos
	
	@Test
	public void AdultoFemBaixoPesoMuitoGraveLimitMinTest() {

		Assert.assertEquals("Baixo Peso - Muito Grave", imc.calcularIMC(1.69, 45, "F", 21));
	}

	@Test
	public void AdultoFemBaixoPesoMuitoGraveLimitMaxTest() {

		Assert.assertEquals("Baixo Peso - Muito Grave", imc.calcularIMC(2.11, 71, "F", 65));
	}

	@Test
	public void AdultoMascascBaixoPesoMuitoGraveLimitMinTest() {

		Assert.assertEquals("Baixo Peso - Muito Grave", imc.calcularIMC(1.69, 45, "M", 21));
	}

	@Test
	public void AdultoMascascBaixoPesoMuitoGraveLimitMaxTest() {

		Assert.assertEquals("Baixo Peso - Muito Grave", imc.calcularIMC(2.11, 71, "M", 65));
	}

	@Test
	public void AdultoFemBaixoPesoGraveLimitMinTest() {

		Assert.assertEquals("Baixo Peso - Grave", imc.calcularIMC(1.65, 45, "F", 21));
	}

	@Test
	public void AdultoFemBaixoPesoGraveLimitMaxTest() {

		Assert.assertEquals("Baixo Peso - Grave", imc.calcularIMC(2.11, 75, "F", 65));
	}

	@Test
	public void AdultoMascascBaixoPesoGraveLimitMinTest() {

		Assert.assertEquals("Baixo Peso - Grave", imc.calcularIMC(1.65, 45, "M", 21));
	}

	@Test
	public void AdultoMascascBaixoPesoGraveLimitMaxTest() {

		Assert.assertEquals("Baixo Peso - Grave", imc.calcularIMC(2.11, 75, "M", 65));
	}

	@Test
	public void AdultoFemBaixoPesoLimitMinTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.57, 45, "F", 21));
	}

	@Test
	public void AdultoFemBaixoPesoLimitMaxTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(2.11, 81, "F", 65));
	}

	@Test
	public void AdultoMascascBaixoPesoLimitMinTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.57, 45, "M", 21));
	}

	@Test
	public void AdultoMascascBaixoPesoLimitMaxTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(2.11, 81, "M", 65));
	}

	@Test
	public void AdultoFemPesoIdealLimitMinTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.45, 45, "F", 21));
	}

	@Test
	public void AdultoFemPesoIdealLimitMaxTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(2.11, 111, "F", 65));
	}

	@Test
	public void AdultoMascPesoIdealLimitMinTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.45, 45, "M", 21));
	}

	@Test
	public void AdultoMascPesoIdealLimitMaxTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(2.11, 111, "M", 65));
	}

	@Test
	public void AdultoFemSobrepesoLimitMinTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.45, 53, "F", 21));
	}

	@Test
	public void AdultoFemSobrepesoLimitMaxTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(2.09, 111, "F", 65));
	}

	@Test
	public void AdultoMascascSobrepesoLimitMinTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.45, 53, "M", 21));
	}

	@Test
	public void AdultoMascascSobrepesoLimitMaxTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(2.09, 111, "M", 65));
	}

	@Test
	public void AdultoFemObesidadeGILimitMinTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.45, 63.1, "F", 21));
	}

	@Test
	public void AdultoFemObesidadeGILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.91, 111, "F", 65));
	}

	@Test
	public void AdultoMascObesidadeGILimitMinTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.45, 63.1, "M", 21));
	}

	@Test
	public void AdultoMascObesidadeGILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.91, 111, "M", 65));
	}

	@Test
	public void AdultoFemObesidadeGIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.45, 75, "F", 21));
	}

	@Test
	public void AdultoFemObesidadeGIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.77, 111, "F", 65));
	}

	@Test
	public void AdultoMascObesidadeGIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.45, 75, "M", 21));
	}

	@Test
	public void AdultoMascObesidadeGIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.77, 111, "M", 65));
	}

	@Test
	public void AdultoFemObesidadeGIIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.45, 85, "F", 21));
	}

	@Test
	public void AdultoFemObesidadeGIIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.65, 111, "F", 65));
	}

	@Test
	public void AdultoMascObesidadeGIIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.45, 85, "M", 21));
	}

	@Test
	public void AdultoMascObesidadeGIIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.65, 111, "M", 65));
	}
	//Casos de Testes da categoria Idosos
	@Test
	public void IdosoFemBaixoPesoLimitMinTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.45, 45, "F", 66));
	}

	@Test
	public void IdosoFemBaixoPesoLimitMaxTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(2.11, 97.9, "F", 66));
	}

	@Test
	public void IdosoMascBaixoPesoLimitMinTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.45, 45, "M", 66));
	}

	@Test
	public void IdosoMascBaixoPesoLimitMaxTest() {

		Assert.assertEquals("Baixo Peso", imc.calcularIMC(2.11, 97.9, "M", 66));
	}

	@Test
	public void IdosoFemPesoIdealLimitMinTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.45, 46.3, "F", 66));
	}

	@Test
	public void IdosoFemPesoIdealLimitMaxTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(2.11, 120.6, "F", 66));
	}

	@Test
	public void IdosoMascPesoIdealLimitMinTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.45, 46.3, "M", 66));
	}

	@Test
	public void IdosoMascPesoIdealLimitMaxTest() {

		Assert.assertEquals("Peso Ideal", imc.calcularIMC(2.11, 120.6, "M", 66));
	}

	@Test
	public void IdosoFemSobrepesoLimitMinTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.45, 57, "F", 66));
	}

	@Test
	public void IdosoFemSobrepesoLimitMaxTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(2.11, 142.8, "F", 66));
	}

	@Test
	public void IdosoMascSobrepesoLimitMinTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.45, 57, "M", 66));
	}

	@Test
	public void IdosoMascSobrepesoLimitMaxTest() {

		Assert.assertEquals("Sobrepeso", imc.calcularIMC(2.11, 133.9, "M", 66));
	}

	@Test
	public void IdosoFemObesidadeGILimitMinTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.45, 67.5, "F", 66));
	}

	@Test
	public void IdosoFemObesidadeGILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(2.11, 165.1, "F", 66));
	}

	@Test
	public void IdosoMascObesidadeGILimitMinTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(1.45, 63.3, "M", 66));
	}

	@Test
	public void IdosoMascObesidadeGILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau I", imc.calcularIMC(2.11, 156.2, "M", 66));
	}

	@Test
	public void IdosoFemObesidadeGIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.45, 78.1, "F", 66));
	}

	@Test
	public void IdosoFemObesidadeGIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(2.11, 186.9, "F", 66));
	}

	@Test
	public void IdosoMascObesidadeGIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(1.45, 73.8, "M", 66));
	}

	@Test
	public void IdosoMascObesidadeGIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau II", imc.calcularIMC(2.11, 178, "M", 66));
	}

	@Test
	public void IdosoFemObesidadeGIIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.45, 88.4, "F", 66));
	}

	@Test
	public void IdosoFemObesidadeGIIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(2.11, 187, "F", 66));
	}

	@Test
	public void IdosoMascObesidadeGIIILimitMinTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(1.45, 84.1, "M", 66));
	}

	@Test
	public void IdosoMascObesidadeGIIILimitMaxTest() {

		Assert.assertEquals("Obesidade Grau III", imc.calcularIMC(2.11, 178.1, "M", 66));
	}
	//Casos de Testes da categoria Infantil
	@Test
	public void InfantilFemBaixoPesoLimitMin2A4Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(0.86, 9.0, "F", 2));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax2A4Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.02, 14.5, "F", 4));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin2A4Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(0.86, 11.1, "F", 2));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax2A4Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.02, 15.50, "F", 4));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin2A4Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(0.86, 13.3, "F", 2));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax2A4Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.02, 18.2, "F", 4));
	}

	@Test
	public void InfantilFemObesidadeLimitMin2A4Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(0.86, 18.8, "F", 2));
	}

	@Test
	public void InfantilFemObesidadeLimitMax2A4Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.02, 50.0, "F", 4));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin4A6Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.08, 15.4, "F", 5));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax4A6Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.13, 17.3, "F", 6));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin4A6Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.08, 16.2, "F", 5));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax4A6Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.13, 17.8, "F", 6));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin4A6Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.08, 19.6, "F", 5));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax4A6Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.13, 21.8, "F", 6));
	}

	@Test
	public void InfantilFemObesidadeLimitMin4A6Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.08, 21.6, "F", 5));
	}

	@Test
	public void InfantilFemObesidadeLimitMax4A6Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.13, 24.2, "F", 6));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin6A8Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.19, 18.60, "F", 7));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax6A8Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.25, 21.2, "F", 8));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin6A8Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.19, 19.6, "F", 7));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax6A8Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.25, 21.9, "F", 8));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin6A8Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.19, 25.8, "F", 7));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax6A8Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.25, 28.6, "F", 8));
	}

	@Test
	public void InfantilFemObesidadeLimitMin6A8Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.19, 28.4, "F", 7));
	}

	@Test
	public void InfantilFemObesidadeLimitMax6A8Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.25, 32.0, "F", 8));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin8A10Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.25, 19.0, "F", 9));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax8A10Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.37, 21.8, "F", 10));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin8A10Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.25, 21.9, "F", 9));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax8A10Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.37, 27.0, "F", 10));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin8A10Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.25, 31.2, "F", 9));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax8A10Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.37, 37.4, "F", 10));
	}

	@Test
	public void InfantilFemObesidadeLimitMin8A10Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.25, 36.0, "F", 9));
	}

	@Test
	public void InfantilFemObesidadeLimitMax8A10Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.37, 43.2, "F", 10));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin10A12Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.38, 26.20, "F", 11));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax10A12Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.49, 32.0, "F", 12));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin10A12Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.38, 33.0, "F", 11));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax10A12Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.49, 34.80, "F", 12));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin10A12Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.38, 38.20, "F", 11));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax10A12Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.49, 48.90, "F", 12));
	}

	@Test
	public void InfantilFemObesidadeLimitMin10A12Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.38, 48.0, "F", 11));
	}

	@Test
	public void InfantilFemObesidadeLimitMax10A12Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.49, 58.0, "F", 12));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin12A14Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.50, 32.50, "F", 13));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax12A14Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.57, 38.20, "F", 14));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin12A14Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.50, 37.2, "F", 13));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax12A14Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.57, 44.0, "F", 14));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin12A14Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.50, 48.60, "F", 13));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax12A14Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.57, 53.40, "F", 14));
	}

	@Test
	public void InfantilFemObesidadeLimitMin12A14Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.50, 59.20, "F", 13));
	}

	@Test
	public void InfantilFemObesidadeLimitMax12A14Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.57, 69.80, "F", 14));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin14A16Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.58, 39.0, "F", 15));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax14A16Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.63, 44.0, "F", 16));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin14A16Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.58, 42.8, "F", 15));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax14A16Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.63, 55.0, "F", 16));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin14A16Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.58, 58.0, "F", 15));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax14A16Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.63, 68.0, "F", 16));
	}

	@Test
	public void InfantilFemObesidadeLimitMin14A16Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.58, 71.0, "F", 15));
	}

	@Test
	public void InfantilFemObesidadeLimitMax14A16Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.63, 79.60, "F", 16));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin16A18Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.59, 40.60, "F", 17));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax16A18Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.68, 48.0, "F", 18));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin16A18Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.59, 48.0, "F", 17));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax16A18Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.68, 62.80, "F", 18));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin16A18Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.59, 63.80, "F", 17));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax16A18Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.68, 79.80, "F", 18));
	}

	@Test
	public void InfantilFemObesidadeLimitMin16A18Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.59, 79.0, "F", 17));
	}

	@Test
	public void InfantilFemObesidadeLimitMax16A18Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.68, 96.0, "F", 18));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMin18A20Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.60, 43.0, "F", 19));
	}

	@Test
	public void InfantilFemBaixoPesoLimitMax18A20Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.74, 54.0, "F", 20));
	}

	@Test
	public void InfantilFemPesoIdealLimitMin18A20Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.60, 49.0, "F", 19));
	}

	@Test
	public void InfantilFemPesoIdealLimitMax18A20Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.74, 73.0, "F", 20));
	}

	@Test
	public void InfantilFemSobrepesoLimitMin18A20Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.60, 64.60, "F", 19));
	}

	@Test
	public void InfantilFemSobrepesoLimitMax18A20Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.74, 92.0, "F", 20));
	}

	@Test
	public void InfantilFemObesidadeLimitMin18A20Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.60, 84.0, "F", 19));
	}

	@Test
	public void InfantilFemObesidadeLimitMax18A20Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.74, 104.0, "F", 20));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin2A4Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(0.86, 10.7, "M", 2));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax2A4Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.02, 14.6, "M", 4));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin2A4Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(0.86, 11.1, "M", 2));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax2A4Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.02, 17.4, "M", 4));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin2A4Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(0.86, 13.4, "M", 2));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax2A4Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.02, 18.5, "M", 4));
	}

	@Test
	public void InfantilMascObesidadeLimitMin2A4Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(0.86, 15, "M", 2));
	}

	@Test
	public void InfantilMascObesidadeLimitMax2A4Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.02, 19, "M", 4));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin4A6Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.08, 15.8, "M", 5));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax4A6Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.13, 17.9, "M", 6));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin4A6Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.08, 17.1, "M", 5));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax4A6Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.13, 20.6, "M", 6));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin4A6Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.08, 19.8, "M", 5));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax4A6Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.13, 23.3, "M", 6));
	}

	@Test
	public void InfantilMascObesidadeLimitMin4A6Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.08, 22, "M", 5));
	}

	@Test
	public void InfantilMascObesidadeLimitMax4A6Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.13, 24.3, "M", 6));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin6A8Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.19, 19, "M", 7));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax6A8Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.25, 21.8, "M", 8));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin6A8Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.19, 21.2, "M", 7));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax6A8Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.25, 26.2, "M", 8));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin6A8Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.19, 24.2, "M", 7));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax6A8Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.25, 29.6, "M", 8));
	}

	@Test
	public void InfantilMascObesidadeLimitMin6A8Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.19, 27, "M", 7));
	}

	@Test
	public void InfantilMascObesidadeLimitMax6A8Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.25, 32.8, "M", 8));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin8A10Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.25, 21.3, "M", 9));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax8A10Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.37, 27, "M", 10));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin8A10Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.25, 22.6, "M", 9));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax8A10Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.37, 32.6, "M", 10));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin8A10Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.25, 29.1, "M", 9));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax8A10Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.37, 39.4, "M", 10));
	}

	@Test
	public void InfantilMascObesidadeLimitMin8A10Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.25, 33, "M", 9));
	}

	@Test
	public void InfantilMascObesidadeLimitMax8A10Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.37, 44, "M", 10));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin10A12Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.38, 27.3, "M", 11));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax10A12Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.49, 33.3, "M", 12));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin10A12Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.38, 29.10, "M", 11));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax10A12Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.49, 42.1, "M", 12));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin10A12Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.38, 38.8, "M", 11));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax10A12Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.49, 51, "M", 12));
	}

	@Test
	public void InfantilMascObesidadeLimitMin10A12Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.38, 44, "M", 11));
	}

	@Test
	public void InfantilMascObesidadeLimitMax10A12Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.49, 57.7, "M", 12));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin12A14Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.50, 34, "M", 13));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax12A14Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.57, 39.4, "M", 14));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin12A14Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.50, 36.1, "M", 13));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax12A14Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.57, 49.7, "M", 14));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin12A14Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.50, 49.5, "M", 13));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax12A14Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.57, 61.6, "M", 14));
	}

	@Test
	public void InfantilMascObesidadeLimitMin12A14Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.50, 56.3, "M", 13));
	}

	@Test
	public void InfantilMascObesidadeLimitMax12A14Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.57, 69, "M", 14));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin14A16Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.58, 40.5, "M", 15));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax14A16Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.63, 44.5, "M", 16));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin14A16Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.58, 42.6, "M", 15));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax14A16Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.63, 58.4, "M", 16));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin14A16Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.58, 58.5, "M", 15));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax14A16Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.63, 71.2, "M", 16));
	}

	@Test
	public void InfantilMascObesidadeLimitMin14A16Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.58, 67, "M", 15));
	}

	@Test
	public void InfantilMascObesidadeLimitMax14A16Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.63, 77.1, "M", 16));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin16A18Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.59, 44, "M", 17));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax16A18Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.68, 5, "M", 18));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin16A18Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.59, 46.1, "M", 17));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax16A18Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.68, 66, "M", 18));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin16A18Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.59, 63.1, "M", 17));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax16A18Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.68, 79.5, "M", 18));
	}

	@Test
	public void InfantilMascObesidadeLimitMin16A18Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.59, 71.4, "M", 17));
	}

	@Test
	public void InfantilMascObesidadeLimitMax16A18Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.68, 86, "M", 18));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMin18A20Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.60, 45, "M", 19));
	}

	@Test
	public void InfantilMascBaixoPesoLimitMax18A20Test() {
		Assert.assertEquals("Baixo Peso", imc.calcularIMC(1.74, 57, "M", 20));
	}

	@Test
	public void InfantilMascPesoIdealLimitMin18A20Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.60, 50, "M", 19));
	}

	@Test
	public void InfantilMascPesoIdealLimitMax18A20Test() {
		Assert.assertEquals("Peso Ideal", imc.calcularIMC(1.74, 75, "M", 20));
	}

	@Test
	public void InfantilMascSobrepesoLimitMin18A20Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.60, 67, "M", 19));
	}

	@Test
	public void InfantilMascSobrepesoLimitMax18A20Test() {
		Assert.assertEquals("Sobrepeso", imc.calcularIMC(1.74, 89, "M", 20));
	}

	@Test
	public void InfantilMascObesidadeLimitMin18A20Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.60, 80, "M", 19));
	}

	@Test
	public void InfantilMascObesidadeLimitMax18A20Test() {
		Assert.assertEquals("Obesidade", imc.calcularIMC(1.74, 97, "M", 20));
	}

	@AfterClass
	protected void tearDown() {

	}
}